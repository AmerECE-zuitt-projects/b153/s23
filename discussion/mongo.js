/* Using MongoDB locally:
    - To use MongoDB on your local machine, you must first open a terminal and type the command "mongo" to run the MongoDB Community Server.

    - The Server must always be running for the duration of your use of your local MongoDB interface/Mongo Shell.

    - Open a separate terminal and type "mongo" to open the Mongo Shell, which is command line interface that allows us to issue MongoDB commands.
*/

/* MongoDB Commands:
    - show dbs - Show list of all databases

    - use myFirstDB - create a database ,, use <database name>, the use command is only the first step in creating a database. Next, you must create a single record for the database to actually be created


*/
// create single document
db.users.insertOne({name: "John Smith", age: 20, isActive: true})

// Create a second user
db.users.insertOne({name: "Jane Doe", age: 21, isActive: false})

// Get a list of all content
db.users.find()

// Within Each database is a number of collections, where multiple related data is stored (users, posts, products, etc)

// Create a collection
// If a collection does not exist yet, MongoDB creates the collection when you first store data for that collection
db.products.insertMany([
    {name: "Product 1", price: 200.50, stock: 100, description: "Lorem Ipsum"},
    {name: "Product 2", price: 333, stock: 25, description: "Lorem Ipsum"}
])

db.products.find({name: "Product 2"})

// Update an existing document:
db.users.updateOne(
    {_id: ObjectId("61fbcaf023f433eefcb79b44")}, // What to update
    {
        $set: {isActive: true} // How to update
    }
)

// Update to add new field:
db.users.updateOne(
    {_id: ObjectId("61fbcaf023f433eefcb79b44")},
    {
        $set: {
            address: {
                street: "123 Street st",
                city: "New York",
                country: "United States"
            }
        }
    }
)

// Format results in a more presentable way:
db.users.find().pretty()

// Delete a document
db.users.deleteOne({_id: ObjectId("61fbca2223f433eefcb79b43")})