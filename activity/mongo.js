db.courses.insertMany([
    {courseName: "Basic HTML", description: "hypertext markup language", price: 10000, isActive: true, enrollees: []},
    {courseName: "Basic CSS", description: "Cascading Style Sheets", price: 10000, isActive: false, enrollees: []},
    {courseName: "Basic JS", description: "JavaScript", price: 20000, isActive: true, enrollees: [{userId: ObjectId("61fbcaf023f433eefcb79b44")}]}
])